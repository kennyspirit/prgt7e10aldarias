package Ejemplo0736;

import java.util.ArrayList;
import java.util.Iterator;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Main {

  public static void main(String args[]) {

    ArrayList<Articulo> articulos = new ArrayList<Articulo>(3);

    int i;

    for (i = 0; i < 3; i++) {
      articulos.add(new Articulo("Articulo" + i));
    }

    // Se debe poner justo despues de rellenar
    Iterator iter = articulos.iterator();
    i = 0;
    while (iter.hasNext()) {
      Articulo articulo = (Articulo) iter.next();
      System.out.println(i + ". " + articulo.getNombre());
      // System.out.println(i + ". " + iter.next().getNombre());
      i++;
    }
  }
}

/* EJECUCION:
 0. Articulo0
 1. Articulo1
 2. Articulo2
 */
