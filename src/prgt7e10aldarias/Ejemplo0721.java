/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e10aldarias;

/**
 * Fichero: Ejemplo0721.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0721 {

  public static void main(String args[]) {
    String str1 = "  ab ";
    String str2;
    System.out.println("Longuitud de [" + str1 + "]: " + str1.length());
    str2 = str1.trim();
    System.out.println("Longuitud de [" + str2 + "]: " + str2.length());
  }
}
/* EJECUCION:
 Longuitud de [  ab ]: 5
 Longuitud de [ab]: 2
 */
