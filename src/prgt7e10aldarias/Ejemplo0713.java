/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e10aldarias;

/**
 * Fichero: Ejemplo0713.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0713 {

  public static void main(String args[]) {
    String str1 = "Pepe";
    String str2 = new String("Paco");
    String str3 = new String(str2);

    // Error. No se permite con caracteres.
    // String str4= {'H','o','l','a'};

    String str4;
    str4 = "Prueba";
    System.out.println(str1);
    System.out.println(str2);
    System.out.println(str3);
    System.out.println(str4);
  }
}
/* EJECUCION:
 Pepe
 Paco
 Paco
 Prueba
 */
