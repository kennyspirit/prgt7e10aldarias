/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e10aldarias;

/**
 * Fichero: Ejemplo0708.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0708 {

  public static void burbuja(int array[]) {
    int aux;
    for (int i = array.length; i > 0; i--) {
      for (int j = 0; j < i - 1; j++) {
        if (array[j] > array[j + 1]) {
          aux = array[j + 1];
          array[j + 1] = array[j];
          array[j] = aux;
        }
      }
    }
  }

  public static void main(String args[]) {
    int array[] = {4, 3, 5, 1};
    for (int i : array) {
      System.out.print(i + " ");
    }
    System.out.println("");
    burbuja(array);
    for (int i : array) {
      System.out.print(i + " ");
    }
    System.out.println("");
  }
}
/* EJECUCION:
4 3 5 1 
1 3 4 5
*/
