/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e10aldarias;

/**
 * Fichero: Ejemplo0715.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0715 {

  public static void main(String args[]) {
    String str1 = "PRUEBA1";
    String str2 = " PRUEBA2";
    String str3;

    System.out.println(str1);
    str1 = str1.concat(str2);
    System.out.println(str1);
    str1 = str1 + str2;
    System.out.println(str1);
    // str3=str3.concat("PRUEBA"); // Error debe inicializarse
    // str3=null; // Error no puede inicializarse a null
    str3 = "";

    str3 = str3.concat("PRUEBA3");
    System.out.println(str3);
  }
}
/* EJECUCION:
 PRUEBA1
 PRUEBA1 PRUEBA2
 PRUEBA1 PRUEBA2 PRUEBA2
 PRUEBA3
 */
