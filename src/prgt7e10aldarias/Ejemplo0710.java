/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e10aldarias;

/**
 * Fichero: Ejemplo0710.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0710 {

  public static void main(String args[]) {

    final int filas = 2;
    final int columnas = 3;
    int m[][];
    m = new int[filas][columnas];
    int contador = 1;

    // guarda
    for (int f = 0; f < filas; f++) {
      for (int c = 0; c < columnas; c++) {
        m[f][c] = contador;
        contador++;
      }
    }

    // Talla de la matriz
    System.out.println("Matriz de " + m.length + " filas y "
            + m[0].length + " columnas");

    // imprime
    for (int f = 0; f < filas; f++) {
      for (int c = 0; c < columnas; c++) {
        System.out.print(m[f][c] + " ");
      }
      System.out.println("");
    }

  }
}
/* EJECUCION:
 Matriz de 2 filas y 3 columnas
 1 2 3 
 4 5 6 
 */
