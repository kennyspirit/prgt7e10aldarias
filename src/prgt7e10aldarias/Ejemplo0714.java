/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e10aldarias;

/**
 * Fichero: Ejemplo0714.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0714 {

  public static void main(String args[]) {
    String str = "HOLA";
    System.out.println("SALUDOS".length());
    System.out.println("SALUDOS  ".length());
    System.out.println("LONGITUD DE " + str + " ES " + str.length());
  }
}
/* EJECUCION:
 7
 9
 LONGITUD DE HOLA ES 4
 */
