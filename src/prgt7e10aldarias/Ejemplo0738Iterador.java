/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e10aldarias;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Fichero: Ejemplo0738Iterador.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-ene-2014
 */
public class Ejemplo0738Iterador {

  public static void main(java.lang.String[] args) {

    // Definimos una ArrayList
    List<String> list = new ArrayList<String>();

    // Añadimos elementos
    list.add("Paco");
    list.add("Juan");
    list.add("Atila");

    // Obtenemos un Iterador y recorremos la lista.
    Iterator iter = list.iterator();
    while (iter.hasNext()) {
      System.out.println(iter.next());
    }

  }
} 
/* EJECUCIÓN:
Paco
Juan
Atila
 */
