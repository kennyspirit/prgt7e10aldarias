/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e10aldarias;

/**
 * Fichero: Ejemplo0729.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0729 {

  public static void main(String args[]) {

    String str;

    str = "  6  ";
    int i;
    // i=Integer.parseInt(str); Error
    i = Integer.parseInt(str.trim());
    System.out.println(i);

    str = "   6.6";
    double d;
    d = Double.valueOf(str).doubleValue();
    System.out.println(d);

  }
}
/* EJECUCION:
 6
 6.6
 */
