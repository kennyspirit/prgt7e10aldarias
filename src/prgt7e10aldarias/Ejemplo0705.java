/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e10aldarias;

/**
 * Fichero: Ejemplo0705.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0705 {

  public static int[] combina(int[] lista1, int[] lista2) {
    int[] lista = new int[lista1.length + lista2.length];
    System.arraycopy(lista1, 0, lista, 0, lista1.length);
    System.arraycopy(lista2, 0, lista, lista1.length, lista2.length);
    return lista;
  }

  public static void mostrar(int x[], int l) {
    for (int i = 0; i < l; i++) {
      System.out.print(x[i] + " ");
    }
    System.out.println("");
  }

  public static void main(String args[]) {
    int a[] = {1, 2};
    int b[] = {3, 4};
    int c[] = {0, 0, 0, 0, 0};

    c = combina(a, b);
    System.out.print("a:");
    mostrar(a, a.length);
    System.out.print("b:");
    mostrar(b, b.length);
    System.out.print("c:");
    mostrar(c, c.length);

  }
}
/* Ejecución
a:1 2 
b:3 4 
c:1 2 3 4 
*/
