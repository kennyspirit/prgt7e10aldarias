/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e10aldarias;

/**
 * Fichero: Ejemplo0732.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0732 {

  public static void main(String args[]) {

    StringBuffer sb1 = new StringBuffer("Paco");
    StringBuffer sb2 = new StringBuffer(10);
    StringBuffer sb3 = new StringBuffer();

    // length, capacity
    System.out.println(sb1 + " Longuitud: " + sb1.length() + " Capacidad:"
            + sb1.capacity());
    System.out.println(sb2 + " Longuitud: " + sb2.length() + " Capacidad:"
            + sb2.capacity());
    System.out.println(sb3 + " Longuitud: " + sb3.length() + " Capacidad:"
            + sb3.capacity());

    String a = "a";
    String b = "b";
    a = "bb";

    a = a + b;

    // append  
    StringBuffer nombre = new StringBuffer("Paco");
    StringBuffer apellidos = new StringBuffer(" Lopez");
    nombre.append(apellidos);
    System.out.println(nombre);
    // insert
    nombre.insert(5, "de ");
    System.out.println(nombre);
    // delete
    nombre.delete(0, nombre.length()); // Borra todo
    nombre.append("Paco");
    // reverse
    System.out.println(nombre.reverse());
  }
}
/* EJECUCION:
 Paco Longuitud: 4 Capacidad:20
 Longuitud: 0 Capacidad:10
 Longuitud: 0 Capacidad:16
 Paco Lopez
 Paco de Lopez

 */
