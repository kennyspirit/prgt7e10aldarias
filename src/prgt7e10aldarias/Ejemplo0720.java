/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e10aldarias;

/**
 * Fichero: Ejemplo0720.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0720 {

  public static void main(String args[]) {
    String str1 = "ab";
    String str2 = "ab";
    String str3 = "AB";
    String str4;
    str4 = str3;

    if (str1 == str2) {
      System.out.println("Diferentes objetos. Iguales " + str1 + " y " + str2);
    }

    if (str3 == str4) {
      System.out.println("Mismo objeto. Iguales " + str3 + " y " + str4);
    }

    if (str2 != str3) {
      System.out.println("Diferentes objetos. Diferentes " + str2 + " y " + str3);
    }


  }
}
/* EJECUCION:
 Diferentes objetos. Iguales ab y ab
 Mismo objeto. Iguales AB y AB
 Diferentes objetos. Diferentes ab y AB
 */
