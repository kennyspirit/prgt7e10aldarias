/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e10aldarias;

/**
 * Fichero: Ejemplo0703.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
//Limites del vector
public class Ejemplo0703 {

  public static void main(String args[]) {
    int v[] = new int[2];
    try {
      v[12] = 3;
    } catch (Exception e) {
      System.out.println(e);
    }
  }
}
/* EJECUCION:
 java.lang.ArrayIndexOutOfBoundsException: 12
 */
