/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt7e10aldarias;

/**
 * Fichero: Ejemplo0722.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0722 {

  public void countLetters(String str) {
    if (str == null) {
      return;
    }
    int counter = 0;
    for (int i = 0; i < str.length(); i++) {
      if (Character.isLetter(str.charAt(i))) {
        counter++;
      }
    }
    System.out.println(str + " contiene " + counter + " letras.");
  }

  public static void main(String[] args) {
    new Ejemplo0722().countLetters("abcde123");
  }
}
/* EJECUCION:
 abcde123 contiene 5 letras.
 */
