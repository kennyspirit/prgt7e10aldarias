package Ejemplo0709a;


import Ejemplo0709a.Articulo;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Main {

  public static void main(String args[]) {

    Articulo articulos[] = new Articulo[3];

    for (int i = 0; i < articulos.length; i++) {
      articulos[i] = new Articulo("Articulo" + i);
    }

    for (int i = 0; i < articulos.length; i++) {
      System.out.println(i + ". " + articulos[i].getNombre());
    }
  }
}
