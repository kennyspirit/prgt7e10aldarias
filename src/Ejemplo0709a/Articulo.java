package Ejemplo0709a;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Fichero: Articulo.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
class Articulo {

  private String nombre;

  Articulo(String n) {
    nombre = n;
  }

  /**
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * @param nombre the nombre to set
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
}
