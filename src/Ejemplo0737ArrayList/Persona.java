package Ejemplo0737ArrayList;

/**
 * Fichero: Persona.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 13-ene-2014
 */
public class Persona {

  public int idPersona;
  public String nombre;
  public int altura;

  public Persona(int idPersona, String nombre, int altura) {

    this.idPersona = idPersona;
    this.nombre = nombre;
    this.altura = altura;
  }

  @Override
  public String toString() {

    return "Persona-> ID: " + idPersona + " Nombre: " + nombre + " Altura: " + altura;

  }
}
